# cpp_safer_error_handling
* inspired by both of the rust and golang programming languages and their way of handing errors </br >
what macros does it have : </br >
  - panic_m ( exit_ret_value, msg ) : make the program panic ( takes an exit value and error message ) </br >
  - source_path_str_m ( ) : return line, function identifier, source file of where the macro was used ( also used in the panic_m and unwrap_m macros ) </br >
  - unwrap_m ( Result_t_struct ) : takes an safer::Result_t struct and if its err memeber was true it will use the panic_m macro to panic </br >
* usage of the Result_t struct data types example : </br >
```cpp
#include "safer.hpp"

auto f1() {
  // some code here
  struct safer::Result_t<int> ret;
  ret.Ok(10); // if the function was sucessful value is 10
  return ret;
}

int main() {
  auto r1 = f1();
  unwrap_m(r1); // will not panic because f1 function did not fail
  std::cout << r1.value << std::endl; // output : 10
  
  // or do the error handling manually if you do not want the program to panic
  if (r1.err) {
    std::cout << "f1 function failed\n";
  } else {
    std::cout << "f1 function executed successfully\n";
  }
  
  // set the error to true meaning now the function is treated as failed for testing
  r1.err = true;
  r1.err_code = -100;
  r1.err_msg = "failed for testing";
  
  // or use this useless function in the struct :)
  if (r1.check_err()) {
    std::cout << "f1 function failed\n";
    std::cout << "exit code : " << r1.err_code << "\n";
    std::cout << "exit message : " << r1.err_msg << "\n";
    std::cout << "did not panic because did not wanted to\n";
  } else {
    std::cout << "f1 function executed successfully\n";
  }
  
  // lambda function that failes
  auto f2 = []() {
    struct safer::Result_t<std::string> ret;
    // specify the program exit value for the panic function and the error message
    ret.Err(-1, "lambda function f2 failed");
    return ret;
  };
  
  auto r2 = f2();
  unwrap_m(r2);
  // panics because the f2 lambda function returned an error and failed
  /* output :
      10
      f1 function executed successfully
      f1 function failed
      exit code : -100
      exit message : failed for testing
      did not panic because did not wanted to
      [!] Paniced at line 46, function main, file code.cpp
        error message : lambda function f2 failed
        exit code : -1

  */
}
```

* heapalloc is a class in the safer namespace which allocates the data on the heap using a shared pointer and RAII style mutexes </br >
the advantage of using this over just a smart pointer is that the heapalloc class makes accessing the value without dead locks and </br >
race conditions for the data  </br >
  - used a mutex to avoid dead locks and race conditions </br >
  - used std::lock_guard whenever locking the mutex ( and when returning a data from the pointer ) </br >
  - used a shared pointer to avoid leaking memory </br >
  - whenever the instance of the heapalloc class goes out of scope its value that it is pointing to gets deallocated automatically
```cpp
#include "safer.hpp"

int main() {
  safer::heapalloc<int> v1(10);
  std::cout << v1.value() << std::endl; // output : 10
  v1.change(11); // changed the pointed value on the heap to 11
  std::cout << v1.value() << std::endl; // output : 11
  int* raw_ptr = v1.raw_ptr(); // raw_ptr returns an raw pointer from the value
                               // (may be unsafe do not try to manually delete the allocated space
                               // just included this feature to give the user more freedom lol)
                               // how to shoot yourself in the foot : delete v1.raw_ptr();
  std::weak_ptr<int> w_ptr = v1.weak_ptr(); // returns a weak pointer from the v1's shared pointer
  std::shared_ptr<int> s_ptr = v1.shared_ptr(); // returns the v1's shared pointer
  // value is deallocated because it went out of the scope it was declared in
}
```
* using panic_m ( ) macro separatley
```cpp
#include "safer.hpp"

int main() {
  panic_m("intentionally made the program to panic", -101);
  /* output :
        [!] Paniced at line 4, function main, file code.cpp
        error message : intentionally made the program to panic
        exit code : -101
  */
}
```
* using source_path_str_m ( ) macro separatley
```cpp
#include "safer.hpp"

int main() {
  std::cout << source_path_str_m() << std::endl;
  // output :
  // line 4, function main, file code.cpp
}
```
