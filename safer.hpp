#include <iostream>
#include <mutex>
#include <memory>

#define source_path_str_m() ({ \
	std::string line = std::to_string(__LINE__); \
	std::string path = "line " + line + ", function " + __FUNCTION__ \
		+ ", file " + __FILE__; \
	path; \
})

#define panic_m(a, b) { \
	do { \
		std::cout << "[!] Paniced at " << source_path_str_m() << "\n"; \
		std::cout << "\terror message : " << a << "\n"; \
		std::cout << "\texit code : " << b << std::endl; \
		exit(b); \
	} while (false); \
}

#define unwrap_m(r) { \
	do { \
		if (r.err) { \
			panic_m(r.err_msg, r.err_code); \
		} \
	} while (false); \
}

namespace safer {
	template <typename T> struct Result_t {
		T value;
		bool err = false;
		int err_code = 0;
		std::string err_msg;
		void Ok(T v) {
			value = v;
			err = false;
			err_code = 0;
			err_msg = "\0";
		}
		void Err(int err_c, std::string err_m) {
			err = true;
			err_code = err_c;
			err_msg = err_m;
		}
		bool check_err() {
			return (err) ? true : false;
		}
	};

	template <typename T> class heapalloc {
		private:
			std::mutex mx;
			std::shared_ptr<T> ptr;
			bool debug;
		public:
			heapalloc(T v, bool _debug = false) {
				std::lock_guard<std::mutex> lock(mx);
				ptr = std::make_shared<T>(v);
				debug = _debug;
			}
			T value() {
				std::lock_guard<std::mutex> lock(mx);
				if (debug) {
					std::cout << "[heapalloc] dereferenced shared pointer\n";
				}
				return *ptr;
			}
			void change(T v) {
				std::lock_guard<std::mutex> lock(mx);
				if (debug) {
					std::cout << "[heapalloc] changed value " << *ptr
						<< " to " << v << "\n";
				}
				ptr = std::make_shared<T>(v);
			}
			T* raw_ptr() {
				std::lock_guard<std::mutex> lock(mx);
				if (debug) {
					std::cout << "[heapalloc] got a raw pointer from the"
						<< " shared pointer : " << ptr.get() << '\n';
				}
				return ptr.get();
			}
			std::weak_ptr<T> weak_ptr() {
				std::lock_guard<std::mutex> lock(mx);
				std::weak_ptr<T> w_ptr;
				w_ptr = ptr;
				std::cout << "[heapalloc] assigned the shared pointer to "
					<< "a weak pointer that returned it for the weak_ptr method\n";
				return w_ptr;
			}
			std::shared_ptr<T> shared_ptr() {
				std::lock_guard<std::mutex> lock(mx);
				if (debug) {
					std::cout << "[heapalloc] returned the shared pointer\n";
				}
				return ptr;
			}
			int count() {
				std::lock_guard<std::mutex> lock(mx);
				if (debug) {
					std::cout << "[heapalloc] returned shared pointer reference count : "
						<< ptr.use_count() << '\n';
				}
				return ptr.use_count();
			}
	};
}
